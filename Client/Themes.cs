using MudBlazor;

namespace TodoStudent.Client;

public class Themes
{
    public static MudTheme Theme = new()
    {
        Palette = new Palette()
        {
            AppbarBackground = new("#4B73EB"),
            Primary = new("#4B73EB"),
        },
        Typography = new()
        {
            Default = new()
            {
                FontFamily = new[] { "Roboto" }
            }
        },
        LayoutProperties = new()
        {
            DefaultBorderRadius = "16px"
        }
    };
}