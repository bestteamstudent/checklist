using System.Net.Http.Headers;
using Majorsoft.Blazor.Extensions.BrowserStorage;
using Refit;
using TodoStudent.Shared.Models;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Client.Helpers;

public class AuthHeaderHelper : DelegatingHandler
{
    private readonly ILocalStorageService LocalStorage;

    public AuthHeaderHelper(ILocalStorageService localStorage)
    {
        LocalStorage = localStorage;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        try
        {
            var authHelperModel = await LocalStorage.GetItemAsync<AuthHelperModel<UserViewModel>>("authHelperModel");
            if (authHelperModel != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", authHelperModel.Token);
                AuthDatas.AuthorizeStatus = true;
            }

            return await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (ApiException e)
        {
            throw;
        }
        catch (Exception e)
        {
            throw;
        }
        finally
        {
        }
    }
}

public static class AuthDatas
{
    public static AuthType AuthType { get; set; } = AuthType.NotSelected;
    public static bool AuthorizeStatus { get; set; } = false;
}