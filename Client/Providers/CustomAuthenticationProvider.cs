using System.Security.Claims;
using Majorsoft.Blazor.Extensions.BrowserStorage;
using Microsoft.AspNetCore.Components.Authorization;
using TodoStudent.Shared.Models;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Client.Providers;

public class CustomAuthenticationProvider : AuthenticationStateProvider
{
    private ClaimsPrincipal? authState;

    private ILocalStorageService LocalStorage { get; set; }

    public CustomAuthenticationProvider(ILocalStorageService localStorage)
    {
        LocalStorage = localStorage;
    }

    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        var authUserModel = await LocalStorage.GetItemAsync<AuthHelperModel<UserViewModel>>("authHelperModel");
        if (authUserModel != null)
        {
            NotifyUserLogin(authUserModel.EntityJSON, false);
        }

        authState ??= Anonymous;

        return new AuthenticationState(authState);
    }

    public void NotifyUserLogin(UserViewModel user, bool notify = true)
    {
        var claims = new List<Claim>
        {
            new(ClaimTypes.Name, user.Name),
            new("FirstNameFirstLetter", (user.Name.Length != 0 ? user.Name[0].ToString() : "Н")),
            new(ClaimTypes.Actor, "User"),
            new(ClaimTypes.NameIdentifier, user.ID.ToString()),
            new(nameof(user.ID), user.ID.ToString())
        };

        authState = new ClaimsPrincipal(new ClaimsIdentity(
            claims, "User"));

        if (notify)
        {
            NotifyUser();
        }
    }

    public async Task NotifyUserLogout()
    {
        authState = Anonymous;
        await LocalStorage.RemoveItemAsync("authHelperModel");

        NotifyUser();
    }

    public void NotifyUser()
    {
        NotifyAuthenticationStateChanged(GetAuthenticationStateAsync());
    }

    private ClaimsPrincipal Anonymous =>
        new(new ClaimsIdentity(new[]
        {
            new Claim(ClaimTypes.Role, "Anonymous")
        }));
}