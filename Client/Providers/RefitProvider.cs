using System.Text.Json;
using System.Text.Json.Serialization;
using Refit;
using TodoStudent.Client.Helpers;
using TodoStudent.Shared;
using TodoStudent.Shared.Interfaces.Refit;

namespace TodoStudent.Client.Providers;

public class RefitProvider
{
    public static void AddRefitInterfaces(IServiceCollection services, Uri url)
    {
        // Initialize AutoMapper.
        services.AddAutoMapper(typeof(Mappings));

        // Add services to the container.

        var serializerOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        // serializerOptions.PropertyNameCaseInsensitive = true;
        // serializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        // serializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        // serializerOptions.Converters.Add(new ObjectToInferredTypesConverter());
        // serializerOptions.Converters.Add(new JsonStringEnumConverter(allowIntegerValues: false));

        var serializer = new SystemTextJsonContentSerializer(serializerOptions);

        var refitSettings = new RefitSettings()
        {
            ContentSerializer = serializer
        };

        var apiUrl = url;

        services.AddTransient<AuthHeaderHelper>();

        services.AddRefitClient<IAuthController>(refitSettings)
            .ConfigureHttpClient(c => c.BaseAddress = new Uri(Environment.GetEnvironmentVariable("AUTH_URL") ?? "https://auth.todo.kaboom.pro"))
            .AddHttpMessageHandler<AuthHeaderHelper>();
        
        services.AddRefitClient<ICore>(refitSettings)
            .ConfigureHttpClient(c =>
                c.BaseAddress =
                    new Uri(Environment.GetEnvironmentVariable("CORE_URL") ?? "https://admin.todo.kaboom.pro"));

        services.AddRefitClient<IUserCheckListReportController>(refitSettings)
            .ConfigureHttpClient(c => c.BaseAddress = apiUrl);

        services.AddRefitClient<IUserTaskReportController>(refitSettings)
            .ConfigureHttpClient(c => c.BaseAddress = apiUrl);
    }
}