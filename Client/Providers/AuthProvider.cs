using Majorsoft.Blazor.Extensions.BrowserStorage;
using Microsoft.JSInterop;
using Refit;
using TodoStudent.Shared.Interfaces.Refit;
using TodoStudent.Shared.Models;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Client.Providers;

public class AuthProvider
{
    public ILocalStorageService LocalStorage { get; set; }
    public IAuthController AuthController { get; set; }
    public IJSRuntime JSRuntime { get; set; }
    public CustomAuthenticationProvider AuthenticationProvider { get; set; }

    public AuthProvider(ILocalStorageService localStorage, IAuthController authController, IJSRuntime jsRuntime, CustomAuthenticationProvider authenticationProvider)
    {
        LocalStorage = localStorage;
        AuthController = authController;
        JSRuntime = jsRuntime;
        AuthenticationProvider = authenticationProvider;
    }
    
    public async Task<UserViewModel?> GetMyUser()
    {
        var user = await LocalStorage.GetItemAsync<AuthHelperModel<UserViewModel>>("authHelperModel");
        if (user == null)
        {
            return null;
        }

        try
        {
        }
        catch (Exception e)
        {
        }

        try
        {
            var res = await UpdateDates();
            if (res != null)
            {
                user = res;
            }

            return user.EntityJSON;
        }
        catch (ApiException e)
        {
        }
        catch (Exception e)
        {
        }

        return user.EntityJSON;
    }
    
    private async Task<AuthHelperModel<UserViewModel>> UpdateDates()
    {
        var user = await LocalStorage.GetItemAsync<AuthHelperModel<UserViewModel>>("authHelperModel");
        if (user == null)
        {
            return null;
        }

        return user;
    }

}