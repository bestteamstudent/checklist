using System.ComponentModel.DataAnnotations;
using TaskStatus = TodoStudent.Shared.Models.TaskStatus;

namespace TodoStudent.Shared.EditModels;

public class UserTaskReportEditModel
{
    [Required]
    public int UserCheckListReportID { get; set; }
    [Required]
    public int TaskID { get; set; }
    public string? Description { get; set; }
    public string? FileUrl { get; set; }
    [Required]
    public TaskStatus Status { get; set; }
}

public class UserCheckListReportEditModel
{
    [Required]
    public Guid UserID { get; set; }
    [Required]
    public int TodoListID { get; set; }
    [Required]
    public int ProductVersionID { get; set; }
    public string PlatformName { get; set; }
    public string PlatformVersion { get; set; }
    public string? Description { get; set; }
    public string? FileUrl { get; set; }
}