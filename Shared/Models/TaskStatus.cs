using System.ComponentModel.DataAnnotations;

namespace TodoStudent.Shared.Models;

/// <summary>
/// Перечисление статусов выполненой задачи. 
/// </summary>
public enum TaskStatus
{
    [Display(Name = "Пройден")]
    Success,
    [Display(Name = "Не пройден")]
    Failed,
    [Display(Name = "Невозможно пройти")]
    Impossible,
    [Display(Name = "Пропущен")]
    Skipped
}