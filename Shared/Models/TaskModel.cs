using Newtonsoft.Json;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Shared.Models;

public class ProductVersionModel
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public string created_at { get; set; }
    public int product_id { get; set; }
}

public class TaskModel
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public object image_url { get; set; }
    public int position { get; set; }

    [JsonIgnore] public bool Expanded { get; set; } = false;
    public HashSet<TaskModel> subtasks { get; set; } = new();

    [JsonIgnore] public int? ReportTaskID { get; set; } = null!;

    [JsonIgnore] public UserTaskReportViewModel? ReportTask { get; set; } = null!;
}

public class CheckListModel
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public int product_id { get; set; }
}

public class ProductModel
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public object url { get; set; }
}

public class CompanyModel
{
    public int id { get; set; }
    public string name { get; set; }
    public string url { get; set; }
    public string description { get; set; }
}