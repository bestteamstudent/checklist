using Refit;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.Models;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Shared.Interfaces.Refit;

public interface IAuthController
{
    [Post("/api/Auth/user")]
    Task<ServiceResponse<AuthHelperModel<UserViewModel>>> AuthUser(UserAuthModel model);
    
    [Post("/api/Auth/user/create")]
    Task<ServiceResponse<AuthHelperModel<UserViewModel>>> CreateUser(UserEditModel model);
    
}