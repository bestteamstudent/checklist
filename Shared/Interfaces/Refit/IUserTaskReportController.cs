using Refit;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Shared.Interfaces.Refit;

public interface IUserTaskReportController
{
    [Get("/api/UserTaskReport")]
    Task<List<UserTaskReportViewModel>> GetAll();
    
    [Get("/api/UserTaskReport/full")]
    Task<List<UserTaskReportViewModel>> GetFull();
    
    [Get("/api/UserTaskReport/{id}")]
    [Headers("Authorization: Bearer")]
    Task<UserTaskReportViewModel> GetByID(int id);
    
    [Get("/api/UserTaskReport/checklist/{id}")]
    [Headers("Authorization: Bearer")]
    Task<List<UserTaskReportViewModel>> GetByCheckListID(int id);
    
    [Post("/api/UserTaskReport")]
    [Headers("Authorization: Bearer")]
    Task<UserTaskReportViewModel> Create(UserTaskReportEditModel model);
    
    [Put("/api/UserTaskReport/{id}")]
    [Headers("Authorization: Bearer")]
    Task<UserTaskReportViewModel> Update(int id, UserTaskReportEditModel model);
    
    [Delete("/api/UserTaskReport/{id}")]
    [Headers("Authorization: Bearer")]
    Task Delete(int id);  
}