using Refit;
using TodoStudent.Shared.Models;

namespace TodoStudent.Shared.Interfaces.Refit;

public interface ICore
{
    [Get("/company/{id}")]
    Task<CompanyModel> GetCompany(int id);

    [Get("/product/products/")]
    Task<List<ProductModel>> GetProducts();
    
    [Get("/product/{productID}/versions/")]
    Task<List<ProductVersionModel>> GetProductVersions(int productID);
    
    [Get("/product/products/?company_id={companyID}")]
    Task<List<ProductModel>> GetProductsByCompanyID(int companyID);
    
    [Get("/task/checklists/")]
    Task<List<CheckListModel>> GetCheckLists();
    
    [Get("/task/checklists/?product_id={productID}")]
    Task<List<CheckListModel>> GetCheckListsByCompanyID(int productID);

    [Get("/task/tasks/?check_list_id={checkListID}&only_leaf={fullLoad}")]
    Task<List<TaskModel>> GetTasksByCheckListID(int checkListID, bool? fullLoad = false);
}