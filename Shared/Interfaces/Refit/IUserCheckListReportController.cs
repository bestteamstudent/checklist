using Refit;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Shared.Interfaces.Refit;

public interface IUserCheckListReportController
{
    [Get("/api/UserCheckListReport")]
    Task<List<UserCheckListReportViewModel>> GetAll();
    
    [Get("/api/UserCheckListReport/full")]
    Task<List<UserCheckListReportViewModel>> GetFull();
    
    [Get("/api/UserCheckListReport/{id}")]
    [Headers("Authorization: Bearer")]
    Task<UserCheckListReportViewModel> GetByID(int id);
    
    [Post("/api/UserCheckListReport")]
    [Headers("Authorization: Bearer")]
    Task<UserCheckListReportViewModel> Create(UserCheckListReportEditModel model);
    
    [Put("/api/UserCheckListReport/{id}")]
    [Headers("Authorization: Bearer")]
    Task<UserCheckListReportViewModel> Update(int id, UserCheckListReportEditModel model);
    
    [Delete("/api/UserCheckListReport/{id}")]
    [Headers("Authorization: Bearer")]
    Task Delete(int id);
}