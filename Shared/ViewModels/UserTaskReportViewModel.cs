using Newtonsoft.Json;
using TodoStudent.Shared.Models;
using TaskStatus = System.Threading.Tasks.TaskStatus;

namespace TodoStudent.Shared.ViewModels;

public class UserTaskReportViewModel
{
    public int ID { get; set; }
    public int UserCheckListReportID { get; set; }
    public UserCheckListReportViewModel? UserCheckListReport { get; set; }
    public int TaskID { get; set; }
    public string? Description { get; set; }
    public string? FileUrl { get; set; }
    public TaskStatus Status { get; set; }
}

public class UserCheckListReportViewModel
{
    public int ID { get; set; }
    public Guid UserID { get; set; }
    public int TodoListID { get; set; }
    public DateTime StartedAt { get; set; }=DateTime.Now;
    public DateTime? FinishedAt { get; set; } // Nullable for unfinished tasks
    public int ProductVersionID { get; set; }
    public string PlatformName { get; set; }
    public string PlatformVersion { get; set; }
    public string? Description { get; set; }
    public string? FileUrl { get; set; }
    
    [JsonIgnore]
    public CheckListModel? CheckList { get; set; }
    
    [JsonIgnore] public ProductVersionModel? ProductVersion { get; set; }
    
    public List<UserTaskReportViewModel>? TaskReports { get; set; } = new();
}