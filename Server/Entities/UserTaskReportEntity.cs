using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TodoStudent.Server.Entities;

/// <summary>
/// Сущность хранения отчёта задач.
/// </summary>
public class UserTaskReportEntity
{
    [Key]
    public int ID { get; set; }
    [Required]
    public int UserCheckListReportID { get; set; }
    [ForeignKey(nameof(UserCheckListReportID))]
    public UserCheckListReportEntity? UserCheckListReport { get; set; }
    [Required]
    public int TaskID { get; set; }
    public string? Description { get; set; }
    public string? FileUrl { get; set; }
    [Required]
    public TaskStatus Status { get; set; }
}