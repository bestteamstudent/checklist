using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace TodoStudent.Server.Entities;

/// <summary>
/// Сущность хранения отчёта чек-листа.
/// </summary>
public class UserCheckListReportEntity
{
    [Key]
    public int ID { get; set; }
    [Required]
    public Guid UserID { get; set; }
    [Required]
    public int TodoListID { get; set; }
    public DateTime StartedAt { get; set; }=DateTime.Now;
    public DateTime? FinishedAt { get; set; } // Nullable for unfinished tasks
    [Required]
    public int ProductVersionID { get; set; }
    public string PlatformName { get; set; }
    public string PlatformVersion { get; set; }
    public string? Description { get; set; }
    public string? FileUrl { get; set; }

    public HashSet<UserTaskReportEntity>? TaskReports { get; set; } = new();
}