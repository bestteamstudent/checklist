using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TodoStudent.Server.Entities;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.Extensions;
using TodoStudent.Shared.Interfaces;

namespace TodoStudent.Server.Services;

public class UserTaskReportService : IService<UserTaskReportEntity, UserTaskReportEditModel, int>
{
    private ServerContext Context;
    private IMapper Mapper;

    public UserTaskReportService(ServerContext context, IMapper mapper)
    {
        Context = context;
        Mapper = mapper;
    }

    public List<UserTaskReportEntity> GetAll()
    {
        return Context.UserTaskReports.AsNoTracking().ToList();
    }

    public string GetAllHash()
    {
        return GetAll().ToHashSHA256();
    }

    public List<UserTaskReportEntity> GetAllFull()
    {
        return Context.UserTaskReports.AsNoTracking().Include(x => x.UserCheckListReport).ToList();
    }

    public string GetAllFullHash()
    {
        return GetAllFull().ToHashSHA256();
    }

    public async Task<UserTaskReportEntity?> GetByID(int id)
    {
        return await Context.UserTaskReports.AsNoTracking().FirstOrDefaultAsync(x => x.ID == id);
    }

    public async Task<List<UserTaskReportEntity>?> GetByCheckListID(int id)
    {
        return Context.UserTaskReports.AsNoTracking().Include(x => x.UserCheckListReport)
            .Where(x => x.UserCheckListReport.TodoListID == id).ToList();
    }

    public async Task<string> GetByIDHash(int id)
    {
        return (await GetByID(id)).ToHashSHA256();
    }

    public async Task<UserTaskReportEntity?> GetByIDFull(int id)
    {
        return await Context.UserTaskReports.AsNoTracking()
            .Include(x => x.UserCheckListReport)
            .FirstOrDefaultAsync(x => x.ID == id);
    }

    public async Task<string> GetByIDFullHash(int id)
    {
        return (await GetByIDFullHash(id)).ToHashSHA256();
    }

    public async Task<UserTaskReportEntity?> Update(int id, UserTaskReportEditModel editModel)
    {
        var entity = Mapper.Map<UserTaskReportEntity>(editModel);
        entity.ID = id;

        // TODO проверки

        Context.Attach(entity);
        Context.Entry(entity).State = EntityState.Modified;

        await Context.SaveChangesAsync();

        return entity;
    }

    public async Task<UserTaskReportEntity?> Create(UserTaskReportEditModel editModel)
    {
        var entity = Mapper.Map<UserTaskReportEntity>(editModel);
        // TODO проверки

        Context.Add(entity);

        await Context.SaveChangesAsync();

        return entity;
    }

    public async Task<bool> Delete(int id)
    {
        var entity = await Context.UserTaskReports.FindAsync(id);
        if (entity == null)
        {
            return false;
        }

        Context.Remove(entity);
        await Context.SaveChangesAsync();

        return true;
    }
}