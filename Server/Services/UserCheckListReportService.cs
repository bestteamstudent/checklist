using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TodoStudent.Server.Entities;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.Extensions;
using TodoStudent.Shared.Interfaces;

namespace TodoStudent.Server.Services;

public class UserCheckListReportService : IService<UserCheckListReportEntity, UserCheckListReportEditModel, int>
{
    private ServerContext Context;
    private IMapper Mapper;

    public UserCheckListReportService(ServerContext context, IMapper mapper)
    {
        Context = context;
        Mapper = mapper;
    }

    public List<UserCheckListReportEntity> GetAll()
    {
        return Context.UserCheckListReports.AsNoTracking().ToList();
    }

    public string GetAllHash()
    {
        return GetAll().ToHashSHA256();
    }

    public List<UserCheckListReportEntity> GetAllFull()
    {
        return Context.UserCheckListReports.AsNoTracking().Include(x => x.TaskReports).ToList();
    }

    public string GetAllFullHash()
    {
        return GetAllFull().ToHashSHA256();
    }

    public async Task<UserCheckListReportEntity?> GetByID(int id)
    {
        return await Context.UserCheckListReports.AsNoTracking().FirstOrDefaultAsync(x => x.ID == id);
    }

    public async Task<string> GetByIDHash(int id)
    {
        return (await GetByID(id)).ToHashSHA256();
    }

    public async Task<UserCheckListReportEntity?> GetByIDFull(int id)
    {
        return await Context.UserCheckListReports.AsNoTracking()
            .Include(x => x.TaskReports)
            .FirstOrDefaultAsync(x => x.ID == id);
    }

    public async Task<string> GetByIDFullHash(int id)
    {
        return (await GetByIDFullHash(id)).ToHashSHA256();
    }

    public async Task<UserCheckListReportEntity?> Update(int id, UserCheckListReportEditModel editModel)
    {
        var entity = Mapper.Map<UserCheckListReportEntity>(editModel);
        entity.ID = id;

        // TODO проверки

        Context.Attach(entity);
        Context.Entry(entity).State = EntityState.Modified;

        Context.Entry(entity).Property(x => x.StartedAt).IsModified = false;

        await Context.SaveChangesAsync();

        return entity;
    }

    public async Task<UserCheckListReportEntity?> Create(UserCheckListReportEditModel editModel)
    {
        var entity = Mapper.Map<UserCheckListReportEntity>(editModel);
        // TODO проверки
        entity.FinishedAt = null;
        Context.Add(entity);

        await Context.SaveChangesAsync();

        return entity;
    }

    public async Task<bool> Delete(int id)
    {
        var entity = await Context.UserCheckListReports.FindAsync(id);
        if (entity == null)
        {
            return false;
        }

        Context.Remove(entity);
        await Context.SaveChangesAsync();

        return true;
    }
}