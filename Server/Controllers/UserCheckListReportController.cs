using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TodoStudent.Server.Services;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserCheckListReportController : ControllerBase
{
    private UserCheckListReportService Service;
    private IMapper Mapper;

    public UserCheckListReportController(UserCheckListReportService service, IMapper mapper)
    {
        Service = service;
        Mapper = mapper;
    }

    // GET: api/UserCheckListReport
    [HttpGet]
    public async Task<ActionResult<List<UserCheckListReportViewModel>>> GetAll()
    {
        var datas = Service.GetAll().Select(x => Mapper.Map<UserCheckListReportViewModel>(x)).ToList();
        return datas;
    }

    [HttpGet("full")]
    public async Task<ActionResult<List<UserCheckListReportViewModel>>> GetFull()
    {
        var datas = Service.GetAllFull().Select(x => Mapper.Map<UserCheckListReportViewModel>(x)).ToList();
        return Ok(datas);
    }

    // GET: api/UserCheckListReport?id=5
    [HttpGet("{id}")]
    public async Task<ActionResult<UserCheckListReportViewModel>> GetByID(int id)
    {
        var data = await Service.GetByID(id);
        if (data == null)
        {
            return NotFound();
        }

        return Ok(Mapper.Map<UserCheckListReportViewModel>(data));
    }

    // POST: api/UserCheckListReport
    [HttpPost]
    public async Task<ActionResult<UserCheckListReportViewModel>> Create([FromBody] UserCheckListReportEditModel editModel)
    {
        var data = await Service.Create(editModel);
        if (data == null)
        {
            return NotFound();
        }

        return Ok(Mapper.Map<UserCheckListReportViewModel>(data));
    }

    // PUT: api/UserCheckListReport/5
    [HttpPut("{id}")]
    public async Task<ActionResult<UserCheckListReportViewModel>> Edit(int id, [FromBody] UserCheckListReportEditModel editModel)
    {
        var data = await Service.Update(id, editModel);
        if (data == null)
        {
            return NotFound();
        }

        return Ok(Mapper.Map<UserCheckListReportViewModel>(data));
    }

    // DELETE: api/UserCheckListReport/5
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        var data = await Service.Delete(id);
        return data ? NoContent() : NotFound("Сущность не найдена");
    }
}