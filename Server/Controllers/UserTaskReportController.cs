using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TodoStudent.Server.Services;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserTaskReportController : ControllerBase
{
    private UserTaskReportService Service;
    private IMapper Mapper;

    public UserTaskReportController(UserTaskReportService service, IMapper mapper)
    {
        Service = service;
        Mapper = mapper;
    }

    // GET: api/UserTaskReport
    [HttpGet]
    public async Task<ActionResult<List<UserTaskReportViewModel>>> GetAll()
    {
        var datas = Service.GetAll().Select(x => Mapper.Map<UserTaskReportViewModel>(x)).ToList();
        return datas;
    }

    [HttpGet("full")]
    public async Task<ActionResult<List<UserTaskReportViewModel>>> GetFull()
    {
        var datas = Service.GetAllFull().Select(x => Mapper.Map<UserTaskReportViewModel>(x)).ToList();
        return Ok(datas);
    }

    // GET: api/UserTaskReport?id=5
    [HttpGet("{id}")]
    public async Task<ActionResult<UserTaskReportViewModel>> GetByID(int id)
    {
        var data = await Service.GetByID(id);
        if (data == null)
        {
            return NotFound();
        }

        return Ok(Mapper.Map<UserTaskReportViewModel>(data));
    }
    
    // GET: api/UserTaskReport?id=5
    [HttpGet("checklist/{id}")]
    public async Task<ActionResult<List<UserTaskReportViewModel>>> GetByCheckListID(int id)
    {
        var data = (await Service.GetByCheckListID(id)).Select(x => Mapper.Map<UserTaskReportViewModel>(x)).ToList();

        return Ok(data);
    }

    // POST: api/UserTaskReport
    [HttpPost]
    public async Task<ActionResult<UserTaskReportViewModel>> Create([FromBody] UserTaskReportEditModel editModel)
    {
        var data = await Service.Create(editModel);
        if (data == null)
        {
            return NotFound();
        }

        return Ok(Mapper.Map<UserTaskReportViewModel>(data));
    }

    // PUT: api/UserTaskReport/5
    [HttpPut("{id}")]
    public async Task<ActionResult<UserTaskReportViewModel>> Edit(int id, [FromBody] UserTaskReportEditModel editModel)
    {
        var data = await Service.Update(id, editModel);
        if (data == null)
        {
            return NotFound();
        }

        return Ok(Mapper.Map<UserTaskReportViewModel>(data));
    }

    // DELETE: api/UserTaskReport/5
    [HttpDelete("{id}")]
    public async Task<ActionResult> Delete(int id)
    {
        var data = await Service.Delete(id);
        return data ? NoContent() : NotFound("Сущность не найдена");
    }
}