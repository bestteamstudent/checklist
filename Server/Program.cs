﻿using Microsoft.AspNetCore.Hosting.StaticWebAssets;
using Microsoft.EntityFrameworkCore;
using TodoStudent.Server;
using TodoStudent.Server.Services;

var builder = WebApplication.CreateBuilder(args);

StaticWebAssetsLoader.UseStaticWebAssets(builder.Environment, builder.Configuration);

// Initialize EF Core.

#region EFCore

var connectionString = Environment.GetEnvironmentVariable("AUTH_CONNECTION_STRING") ??
                       "Host=portainer.main.kaboom.pro;Database=todo_main;Username=postgres;Password=devversionsuck";

builder.Services.AddDbContext<ServerContext>(options => options.UseNpgsql(connectionString));

#endregion

// AutoMapper.
builder.Services.AddAutoMapper(typeof(Mappings));

#region Services

builder.Services.AddTransient<UserTaskReportService>();
builder.Services.AddTransient<UserCheckListReportService>();

#endregion

// Add services to the container.

builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
    app.UseSwagger();
    app.UseSwaggerUI();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();

app.UseRouting();


app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

// Create DB.

var scope = app.Services.CreateScope();
var db = scope.ServiceProvider.GetRequiredService<ServerContext>();
await db.Database.EnsureCreatedAsync();

app.Run();