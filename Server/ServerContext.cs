using Microsoft.EntityFrameworkCore;
using TodoStudent.Server.Entities;

namespace TodoStudent.Server;

public class ServerContext: DbContext
{
    public DbSet<UserCheckListReportEntity> UserCheckListReports => Set<UserCheckListReportEntity>();
    public DbSet<UserTaskReportEntity> UserTaskReports => Set<UserTaskReportEntity>();
    
    public ServerContext()
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    }

    public ServerContext(DbContextOptions<ServerContext> options) : base(options)
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    }
}
