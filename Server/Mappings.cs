using AutoMapper;
using TodoStudent.Server.Entities;
using TodoStudent.Shared.EditModels;
using TodoStudent.Shared.ViewModels;

namespace TodoStudent.Server;

public class Mappings : Profile
{
    public Mappings()
    {
        CreateMap<UserCheckListReportViewModel, UserCheckListReportEditModel>().ReverseMap();
        CreateMap<UserCheckListReportViewModel, UserCheckListReportEntity>().ReverseMap();
        CreateMap<UserCheckListReportEditModel, UserCheckListReportEntity>().ReverseMap();

        CreateMap<UserTaskReportViewModel, UserTaskReportEditModel>().ReverseMap();
        CreateMap<UserTaskReportViewModel, UserTaskReportEntity>().ReverseMap();
        CreateMap<UserTaskReportEditModel, UserTaskReportEntity>().ReverseMap();
    }
}